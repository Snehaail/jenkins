#!/bin/bash
# Jenkins job configuration script

# Pull the latest changes from the repository
docker-compose down
git pull origin main
docker-compose up -d --build
