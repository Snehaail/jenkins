# Assignment 3: Free Style Job with Jenkins

## Objective
Set up a free style job in Jenkins to take a git pull of dockerrise application created in assignment 2.

## Repository URL
[GitLab Repository](https://gitlab.com/Snehaail/jenkins)

## Jenkins Setup
- Jenkins URL: [Jenkins](http://98.70.32.170:8080/)
- Configured Jenkins to connect to the Git repository.
- Created a Freestyle Job to pull, build, and launch the Dockerized application.

## Build Steps in Jenkins
- Pull the latest code from GitLab.
- Stop existing containers: `docker-compose down`
- Build and run the application: `docker-compose up -d --build`

## Verification
- Application runs on (http://98.70.32.170:5003)
- Changes in the repository reflect in the running application.
